class TestHelloWorldOperator(unittest.TestCase):
    def test_init(self):
        operator = virtual_python_plugin.PythonVirtualenvOperator(system_site_packages=True, task_id='dummyTask',
                                                                  python_callable=foo)
        self.assertEqual(operator.system_site_packages, True)
        self.assertEqual(operator.task_id, 'dummyTask')
        self.assertEqual(operator.python_callable, foo)

    def test_no_system_site_packages(self):
        expected = ['python3', '/usr/local/airflow/.local/lib/python3.7/site-packages/virtualenv', '/tmp/foo']
        operator = virtual_python_plugin.PythonVirtualenvOperator(system_site_packages=False, task_id='dummyTask',
                                                                  python_callable=foo)
        result = operator._generate_virtualenv_cmd('/tmp/foo')
        self.assertEqual(expected, result)
